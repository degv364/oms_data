# OMS : Object Model System

In this repository you will find some configuration files that can be used with OMS. 

## Installation
---

At the repo root execute:

    ./configure.sh configure

This will copy the configuration files to ~/.oms_data. Done, it is installed and ready to use.
See the OMS package to know how.

home directory: ~/.oms_data Here we store the configuration files that OMS needs. 


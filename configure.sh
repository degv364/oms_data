#!/bin/bash

# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory,
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


echo "Configuring the Object Model System (OMS) Data"

if [ "$1" == "configure" ];
then
    echo "Installing OMS data..."

    if [ ! -d $HOME/.oms_data ];
    then
	echo "Creating OMS configuration directory ...";
	mkdir $HOME/.oms_data;
    fi
    
    if [ ! -d $HOME/.oms_data/default ];
    then
	echo "Creating default configuration directory ...";
	mkdir $HOME/.oms_data/default;
    fi
    
    echo "Copying default configuration ...";
    cp -r data/* $HOME/.oms_data/default;

    echo "OMS is configured!"
fi

if [ "$1" == "remove" ];
then
    echo "Uninstalling OMS .."

    if [ -d $HOME/.oms_data/default ];
    then
	echo "Removing default configuration files ..."
	rm -rf $HOME/.oms_data/default
    fi
    echo "Default configurations files have been removed"
fi

if [ "$1" == "purge" ];
then
    echo "Purging OMS ..."
    if [ -d $HOME/.oms_data ];
    then
	echo "Removing ALL configuration files ...";
	rm -rf $HOME/.oms_data/
    fi
    echo "OMS data is completely gone :("
fi
